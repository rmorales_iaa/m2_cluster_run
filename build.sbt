//-----------------------------------------------------------------------------
// Use the same scala version for running application and spark
// To be sure, download spark source and check the scala version in directory "build/·
lazy val myScalaVersion =  "2.13.14"

lazy val programName    = "m2_cluster_run"
lazy val programVersion = "0.0.1"
lazy val authorList     = "Rafael Morales Muñoz (rmorales@iaa.es)"
lazy val license        = "This project is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)"
//-----------------------------------------------------------------------------
//common settings
//version will be calculated using git repository
lazy val commonSettings = Seq(
  name                 := programName
  , version            := programVersion
  , scalaVersion       := myScalaVersion
  , description        := "Apache Spark for FITS standard"
  , organization       := "IAA-CSIC"
  , homepage           := Some(url("http://www.iaa.csic.es"))
)
//-----------------------------------------------------------------------------
//Main class
lazy val mainClassName = "com.m2_cluster_run.Main"
//-----------------------------------------------------------------------------
//dependencies versions
val apacheCommonsIO          = "2.16.1"
val apacheCommonsMath        = "3.6.1"
val logApacheApiScalaVersion = "13.1.0"
val logApacheCoreVersion     = "2.20.0"
val logLogbackVersion        = "1.5.8"
val logSlf4jApiVersion       = "2.0.9"
val mongoScalaDriverVersion  = "5.2.0"
val scallopVersion           = "5.1.0"
val scalaReflectVersion      = myScalaVersion
val scalaTestVersion         = "3.3.0-SNAP4"
val typeSafeVersion          = "1.4.3"
//-----------------------------------------------------------------------------
//external source code
excludeFilter in (Compile, unmanagedSources) := {
  new SimpleFileFilter(f=> {
    val p = f.getCanonicalPath

    p.contains("dataType/tree") ||
    p.contains("database/fortran") ||
    p.contains("database/mariaDB") ||
    p.contains("database/mongoDB/database") ||
    p.contains("database/postgreDB") ||
    p.contains("database/simbad") ||
    p.contains("database/vizier") ||
    p.contains("util/computerInfo") ||
    p.contains("util/mail") ||
    p.contains("util/parser")

  })}
//-----------------------------------------------------------------------------
//resolvers
//sbt-plugin
resolvers ++= Resolver.sonatypeOssRepos("public")
//-----------------------------------------------------------------------------
//https://github.com/benblack86/finest
//To help improve code quality the following compiler flags will provide errors for deprecated and lint issues
scalacOptions ++= Seq(
  "-Xlint",
  "-deprecation",
  "-feature",
  "-unchecked"
  //  "-Xfatal-warnings"   //generate an error when warning raises. Enable when stable version was published
)
//-----------------------------------------------------------------------------
//assembly
assembly / mainClass := Some(mainClassName)
assembly / logLevel  := Level.Warn

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.last
}
//-----------------------------------------------------------------------------
//tests
test / parallelExecution  := false
//-----------------------------------------------------------------------------
//git versioning: https://github.com/sbt/sbt-git

git.useGitDescribe := true
git.uncommittedSignifier := None         //do not append suffix is some changes are not pushed to git repo
git.gitDescribePatterns := Seq("v*.*")   //format of the tag version

//https://stackoverflow.com/questions/56588228/how-to-get-git-gittagtoversionnumber-value
git.gitTagToVersionNumber := { tag: String =>
  if (tag.isEmpty)  None
  else Some(tag.split("-").toList.head)
}

//See configuration options in: https://github.com/sbt/sbt-buildinfo

//package and name of the scala code auto-generated
buildInfoPackage := "BuildInfo"
buildInfoObject := "BuildInfo"

//append build time
buildInfoOptions += BuildInfoOption.BuildTime

//append extra options to be generated
buildInfoKeys ++= Seq[BuildInfoKey](
  buildInfoBuildNumber
  , scalaVersion
  , sbtVersion
  , name
  , version
  , "authorList" -> authorList
  , "license" -> license
  , description
  , organization
  , homepage
  , BuildInfoKey.action("myGitCurrentBranch") {git.gitCurrentBranch.value}
  , BuildInfoKey.action("myGitHeadCommit")    {git.gitHeadCommit.value.get}
)
//-----------------------------------------------------------------------------
//output path for JNI C headers. Generate it with sbt javah
javah / target :=  sourceDirectory.value / "../native/header"
//-----------------------------------------------------------------------------
//dependencies list
lazy val dependenceList = Seq(

  //manage configuration files. https://github.com/typesafehub/config
  "com.typesafe" % "config" % typeSafeVersion

  //logging
  , "ch.qos.logback" % "logback-classic" % logLogbackVersion
  , "org.apache.logging.log4j" %% "log4j-api-scala" % logApacheApiScalaVersion
  , "org.apache.logging.log4j" % "log4j-core" % logApacheCoreVersion

  // https://mvnrepository.com/artifact/commons-io/commons-io
  , "commons-io" % "commons-io" % apacheCommonsIO

  //https://mvnrepository.com/artifact/org.apache.commons/commons-math3
  , "org.apache.commons" % "commons-math3" % apacheCommonsMath

  //command line parser : https://github.com/scallop/scallop
  , "org.rogach" %% "scallop" % scallopVersion

  //scala reflect
  , "org.scala-lang" % "scala-reflect" % scalaReflectVersion

  //mongoDB
  , "org.mongodb.scala" %% "mongo-scala-driver" % mongoScalaDriverVersion

  //scala test
  , "org.scalatest" %% "scalatest" % scalaTestVersion % Test
)
//=============================================================================
//root project
lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .enablePlugins(AssemblyPlugin, BuildInfoPlugin, GitVersioning)
  .settings(libraryDependencies ++= dependenceList)
//-----------------------------------------------------------------------------
//End of file 'build.sbt'
//-----------------------------------------------------------------------------
