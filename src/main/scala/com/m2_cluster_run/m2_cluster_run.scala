/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Feb/2020
 * Time:  00h:44m
 * Description: None
 */
//=============================================================================
package com.m2_cluster_run
//=============================================================================
import ch.qos.logback.classic.{Level, LoggerContext}
import com.common.configuration.MyConf
import com.common.util.time.Time
import com.m2_cluster_run.commandLine.CommandLineParser
import com.common.logger.MyLogger
import com.common.util.string.MyString
import com.common.util.time.Time._
import com.m2_cluster_run.commands.BuildCoordFile
import com.m2_cluster_run.commands.m2.M2_ComputingTask
//=============================================================================
import java.time.Instant
import org.slf4j.LoggerFactory
//=============================================================================
//=============================================================================
object m2_cluster_run extends MyLogger {
  //---------------------------------------------------------------------------
  val COMMAND_INFO_LEFT_SIDE_SIZE = 50
  //---------------------------------------------------------------------------
  private final val M2_DEPLOY_DIR  = MyConf.c.getString("Cluster.deployDir.m2")

  def getMpoComposedName(objectName: String) = {
    info(s"Getting the composed name of mpo: '$objectName'")
    val m2_command = s"java -jar m2.jar --web $objectName  --exists"
    val processBuilder = sys.process.Process(Seq("bash", "-c", s"cd $M2_DEPLOY_DIR && $m2_command"))
    val result = processBuilder.!!
    val line = result.split("\n").filter(_.contains("mpo composed name"))
    if (line.isEmpty) ""
    else line.head.split(":").last.replaceAll("'", "").trim
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
import m2_cluster_run._
case class m2_cluster_run() extends App with MyLogger {
  //---------------------------------------------------------------------------
  private var lastCommand = ""
  //---------------------------------------------------------------------------
  private def reduceLoggerVerbose() = {
    val loggerContext = LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext]
    //disable all libraries that starts with org (like spark) to logging up to level error
    loggerContext.getLogger("org.mongodb.driver").setLevel(Level.WARN)
  }
  //---------------------------------------------------------------------------
  private def initialActions(): Unit = {
    // set time zone to UTC
    Instant.now.atZone(zoneID_UTC)

    reduceLoggerVerbose()
  }
  //---------------------------------------------------------------------------
  private def finalActions(startMs: Long): Unit = {
    warning(s"m2_cluster_run elapsed time: ${Time.getFormattedElapseTimeFromStart(startMs)}")
  }
  //---------------------------------------------------------------------------
  private def commandVersion(): Boolean = {
    lastCommand = CommandLineParser.COMMAND_VERSION
    println(Version.value)
    true
  }

  //---------------------------------------------------------------------------
  private def commandM_2(cl: CommandLineParser): Boolean = {
    lastCommand = cl.m2()

    val objectName     = cl.m2()
    val inputDir       = cl.dir()

    val vr  = cl.vr()
    val dropPhotometry       = cl.dropPhotometry.isSupplied
    val debugStorage         = cl.debugStorage.isSupplied
    val onlyPhotometry       = cl.onlyPhotometry.isSupplied
    val onlyWcsFitting       = cl.onlyWcsFitting.isSupplied
    val nodeCount            = if (cl.nodeCount.isSupplied) Some(cl.nodeCount()) else None
    val nodeThreadCount      = if (cl.nodeThreadCount.isSupplied) Some(cl.nodeThreadCount()) else None
    val nodeThreadMultiplier = if (cl.nodeThreadMultiplier.isSupplied) Some(cl.nodeThreadMultiplier()) else None
    val totalThreadCount     = if (cl.totalThread.isSupplied) Some(cl.totalThread()) else None


    val runAsWorker     = cl.worker.isSupplied

    info(s"${MyString.rightPadding("\tobject name", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$objectName'")
    info(s"${MyString.rightPadding("\tinput directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$inputDir'")

    if (nodeCount.isDefined)
      info(s"${MyString.rightPadding("\tnode count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${nodeCount.get}'")
    if (nodeThreadCount.isDefined)
      info(s"${MyString.rightPadding("\tnode thread count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${nodeThreadCount.get}'")
    if (totalThreadCount.isDefined)
      info(s"${MyString.rightPadding("\ttotal thread count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${totalThreadCount.get}'")

    info(s"${MyString.rightPadding("\tphotometric V-R", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$vr'")
    info(s"${MyString.rightPadding("\tdrop only photometry calculations", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$dropPhotometry'")
    info(s"${MyString.rightPadding("\tusing debug storage in the database", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$debugStorage'")
    info(s"${MyString.rightPadding("\trun as computing node worker", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$runAsWorker'")
    info(s"${MyString.rightPadding("\tcalculate only wcs fitting", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$onlyWcsFitting'")
    info(s"${MyString.rightPadding("\tcalculate only photometry", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$onlyPhotometry'")

    val composedName = getMpoComposedName(objectName)
    if (composedName.isEmpty) return error(s"Unknown object '$objectName'")

    if (!runAsWorker)
     BuildCoordFile.build(
       objectName
        , composedName
        , inputDir
        , vr)

    val computingTask = M2_ComputingTask(
        composedName
      , inputDir
      , nodeThreadCount
      , debugStorage
      , dropPhotometry
      , onlyWcsFitting
      , onlyPhotometry
      , runAsWorker)

    computingTask.run(runAsWorker
                      , nodeCount.getOrElse(0)
                      , nodeThreadCount.getOrElse(0)
                      , nodeThreadMultiplier.getOrElse(1)
                      , totalThreadCount.getOrElse(0))
    true
  }

  //---------------------------------------------------------------------------
  def commandManager(cl: CommandLineParser): Boolean = {
    if (cl.version.isSupplied) return commandVersion()
    if (cl.m2.isSupplied) return commandM_2(cl)
    error("Unknown command")
    false
  }
  //---------------------------------------------------------------------------
  def
  run(cl: CommandLineParser): Unit = {
    val startMs = System.currentTimeMillis

    initialActions()

    commandManager(cl)
    //SandBox.sparkScriptBuild()

    finalActions(startMs)
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SparkFits.scala
//=============================================================================
