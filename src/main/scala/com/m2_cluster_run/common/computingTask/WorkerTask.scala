/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Oct/2023
 * Time:  11h:27m
 * Description: None
 */
package com.m2_cluster_run.common.computingTask
//=============================================================================
//=============================================================================
trait WorkerTask extends BaseTask
//=============================================================================
//End of file WorkerTask.scala
//=============================================================================