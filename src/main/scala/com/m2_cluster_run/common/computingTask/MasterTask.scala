/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Oct/2023
 * Time:  11h:27m
 * Description: None
 */
package com.m2_cluster_run.common.computingTask
//=============================================================================
import com.m2_cluster_run.common.computingNode.ComputingNode
//=============================================================================
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
//=============================================================================
//=============================================================================
object MasterTask {
  //---------------------------------------------------------------------------
  final private val WORKER_TASK_START_RANDOM_WAIT_MS = 200
  //---------------------------------------------------------------------------
}
//=============================================================================
import MasterTask._
trait MasterTask extends BaseTask {
  //---------------------------------------------------------------------------
  val computingNodeSeq: List[ComputingNode]
  val totalNodeCount = computingNodeSeq.size
  val totalThreadCount = (computingNodeSeq map (t => t.threadCount)).sum
  //---------------------------------------------------------------------------
  def runWorker(computingNode: ComputingNode
                , nodeCount: Int
                , nodeThreadCount: Int
                , nodeThreadMultiplier: Int
                , totalThreadCount: Int): Unit
  //---------------------------------------------------------------------------
  def jointPartialResults(): Unit
  //---------------------------------------------------------------------------
  protected def runWorkerSeq(): Unit = {

    val futures = computingNodeSeq.map { computingNode =>
      Future {
        Thread.sleep(random.nextInt(WORKER_TASK_START_RANDOM_WAIT_MS))
        runWorker(computingNode
                   , totalNodeCount
                   , computingNode.threadCount
                   , computingNode.threadCountMultiplier
                   , totalThreadCount)
        warning(s"Finished task on worker: ${computingNode.toString()}")
      }
    }

    // Wait for all futures to complete
    val allDoneFuture = Future.sequence(futures)

    // Block until all tasks are completed
    Await.result(allDoneFuture, Duration.Inf)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MasterTask.scala
//=============================================================================