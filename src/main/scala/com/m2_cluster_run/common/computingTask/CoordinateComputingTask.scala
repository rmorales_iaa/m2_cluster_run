/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Oct/2023
 * Time:  11h:23m
 * Description: None
 */
//=============================================================================
package com.m2_cluster_run.common.computingTask
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import java.io.{File, RandomAccessFile}
import java.nio.channels.FileLock
import java.util.Random
//=============================================================================
//=============================================================================
object CoordinateComputingTask extends MyLogger {
  //---------------------------------------------------------------------------
  def runRemoteSSHCommand(host: String, command: String): Boolean = {
    val sshCommand = s"ssh $host '$command'"
    val processBuilder = sys.process.Process(Seq("bash", "-c", sshCommand))
    processBuilder.!!
    true
  }
  //---------------------------------------------------------------------------
  def run(filePath: String
          , process: (Long, Long) => Unit
          , nodeCount: Int
          , nodeThreadCount: Int
          , nodeThreadMultiplier: Int
          , totalThreadCount: Int
          , minRandomWaitMs: Int = 100
          , maxRandomWaitMs: Int = 200): Unit = {
    //------------------------------------------------------------------------
    var raFile: RandomAccessFile = null
    var lock: FileLock = null
    var continue = true

    //random wait
    val random = new Random()

    //------------------------------------------------------------------------
    def getChunkSize() = nodeThreadCount * nodeThreadMultiplier
    //------------------------------------------------------------------------
    def closeLock() = {
      if (lock != null && lock.isValid) {
        lock.release()
        lock = null
      }
    }
    //------------------------------------------------------------------------
    try {
      raFile = new RandomAccessFile(new File(filePath), "rw")
      if (raFile == null) {
        error(s"Can not load file '$filePath'")
        closeLock()
        return
      }
      while (continue) {

        //block other threads
        lock = raFile.getChannel.lock()

        if (lock == null) {
          error(s"Can not lock file '$filePath'")
          closeLock()
          return
        }

        val currentValue = raFile.readLong()
        raFile.seek(0) //back to the start

        if (currentValue <= 0) {
          continue = false; closeLock()
        }
        else {
          //calculate the min and max values to calculate
          val maxValue = currentValue
          var newValue = currentValue - getChunkSize()
          var minValue = newValue + 1

          //check in there are enough values
          if (newValue <= 0) {
            newValue = 0
            minValue = 1
            continue = false
          }

          //write new value
          raFile.writeLong(newValue)
          raFile.seek(0) //back to the start
          info(s"Taken from: $minValue to $maxValue both included. Remain: $newValue")

          //allow that other thread can be executed
          closeLock()

          //call back to the processing method
          process(minValue, maxValue)
        }
        Thread.sleep(minRandomWaitMs + random.nextInt(maxRandomWaitMs - minRandomWaitMs + 1))
      }
    }
    catch {
      case e: Exception => exception(e, s"Error processing file '$filePath'")
        error(e.getStackTrace.mkString("\n"))
      case _: Throwable => error("Got some other kind of throwable exception")
    }
    finally {
      closeLock()
      if (raFile != null) raFile.close()
    }
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file CoordinateNode.scala
//=============================================================================