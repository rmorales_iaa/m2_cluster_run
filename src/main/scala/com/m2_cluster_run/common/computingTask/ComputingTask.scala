/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Oct/2023
 * Time:  11h:06m
 * Description: None
 */
//=============================================================================
package com.m2_cluster_run.common.computingTask
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import java.net.InetAddress
import scala.util.Random
//=============================================================================
//=============================================================================
trait BaseTask extends MyLogger {
  //---------------------------------------------------------------------------
  val nodeName = InetAddress.getLocalHost.getHostName
  //---------------------------------------------------------------------------
  val random = new Random()
  //---------------------------------------------------------------------------
  def run(nodeCount: Int
          , nodeThreadCount: Int
          , nodeThreadMultiplier: Int
          , totalThreadCount: Int): Unit
  //---------------------------------------------------------------------------
}
//=============================================================================
trait ComputingTask extends MyLogger {
  //---------------------------------------------------------------------------
  val masterTask: MasterTask
  val workerTask: WorkerTask
  //---------------------------------------------------------------------------
  def run(runAsWorker: Boolean
          , nodeCount: Int
          , nodeThreadCount: Int
          , nodeThreadMultiplier: Int
          , totalThreadCount: Int):Unit =
    if (runAsWorker) workerTask.run(nodeCount
                                    , nodeThreadCount
                                    , nodeThreadMultiplier
                                    , totalThreadCount)
    else masterTask.run(nodeCount
                        , nodeThreadCount
                        , nodeThreadMultiplier
                        , totalThreadCount)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ComputingTask.scala
//=============================================================================