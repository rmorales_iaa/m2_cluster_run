/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Oct/2023
 * Time:  11h:27m
 * Description: None
 */
package com.m2_cluster_run.common.mongoDB
//=============================================================================
import com.common.database.mongoDB.MongoDB
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
//=============================================================================
case class MyMongoDB(databaseName: String, collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = init()
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  //---------------------------------------------------------------------------
  connected = true
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MyMongoDB.scala
//=============================================================================