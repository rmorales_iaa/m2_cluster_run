/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Oct/2023
 * Time:  14h:27m
 * Description: None
 */
package com.m2_cluster_run.common.computingNode
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object ComputingNode extends MyLogger {
  //---------------------------------------------------------------------------
  def getComputingNodeSeq() = {
    MyConf.c.getStringSeq("Cluster.ComputingNode.activeComputingNodeSeq").map { nodeName =>
      ComputingNode(nodeName)
    }
  }
  //---------------------------------------------------------------------------
  def apply(nodeName:String): ComputingNode = {
    info(s"Reading configuration of computing node: '$nodeName'")
    val key = s"Cluster.ComputingNode.$nodeName"
    ComputingNode(nodeName
                  , MyConf.c.getInt(s"$key.threadCount")
                  , MyConf.c.getIntWithDefaultValue(s"$key.threadCountMultiplier",1))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class ComputingNode(nodeName: String
                         , threadCount: Int
                         , threadCountMultiplier: Int) {
  //---------------------------------------------------------------------------
  override def toString() = s"'$nodeName' (threads:$threadCount and multiplier: $threadCountMultiplier)"
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ComputingNode.scala
//=============================================================================