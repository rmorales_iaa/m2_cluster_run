/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  30/Sep/2023
 * Time:  19h:09m
 * Description: None
 */
package com.m2_cluster_run
//=============================================================================
import com.common.logger.MyLogger

import java.io.{BufferedWriter, File, FileWriter}
import scala.io.Source
//=============================================================================
//=============================================================================
object SandBox extends MyLogger {
  //---------------------------------------------------------------------------
  def sparkScriptBuild() = {

    val inputCsv = "/home/rafa/Downloads/deleteme/list.csv"
    val outputScript = "/home/rafa/proyecto/m2_cluster_run/deploy/spark_remain.bash"
    val defaultVR = "0.5"


    val script_1 ="""
        |#!/bin/bash
        |#----------------------------------------------
        |#set -x #debug enabled
        |#----------------------------------------------
        |OUTPUT_DIR=/mnt/uxmal_groups/spark/tmp/
        |#----------------------------------------------
        |PAIR=""
        |#----------------------------------------------
        |# pair list: object,V-R,mpo_id|
        |""".stripMargin

    val script_2 ="""
        |#-----------------------------------------------
        |function run_java() {
        |
        |  IFS=',' read -r -a parts <<< "$PAIR"
        |  OBJECT="${parts[0]}"
        |  VR="${parts[1]}"
        |  ID="${parts[2]}"
        |
        |  echo "Processing:'$OBJECT' with V-R:'$VR' and ID:'$ID'"
        | # java -jar  m2_cluster_run.jar  --m2 "$OBJECT" --dir "$OUTPUT_DIR/$ID"  --vr "$VR"
        |  #java -jar  m2_cluster_run.jar  --m2 "$OBJECT" --dir "$OUTPUT_DIR/$ID"  --vr "$VR" --only-wcs-fitting
        |  #java -jar  m2_cluster_run.jar  --m2 "$OBJECT" --dir "$OUTPUT_DIR/$ID"  --vr "$VR" --drop-photometry --only-photometry
        |}
        |#-----------------------------------------------
        |
        |if [ $# -eq 0 ]; then
        |  for pair in "${pairs[@]}"; do
        |    PAIR="$pair"
        |    echo "Running: '$PAIR'"
        |    run_java
        |  done
        |else
        |  index=$(( $1 * 2 ))
        |  PAIR="${pairs[$index]}"
        |  echo "Running only index '$1' with value: '$PAIR'"
        |  run_java
        |fi
        |#----------------------------------------------
        |#end of file
        |#----------------------------------------------
        |
        |""".stripMargin


    val bw = new BufferedWriter(new FileWriter(new File(outputScript)))
    bw.write(script_1 + "\n")

    val bufferedSource = Source.fromFile(inputCsv)
    var lineCount = 0
    for (line <- bufferedSource.getLines) {
      val s = line.replaceAll("\\\"","").toLowerCase()
      val seq = s.split(",")
      val objectName = if (seq.isEmpty) line.trim else seq(0).trim
      val r = m2_cluster_run.getMpoComposedName(objectName)

      if (lineCount == 0) bw.write("pairs=(")
      else bw.write("       ")
      bw.write(s""""$objectName,$defaultVR,${r.trim}" , \\\n""")
      lineCount += 1
    }
    bw.write("      )\n")
    bufferedSource.close

    bw.write(script_2 + "\n")
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SandBox.scala
//=============================================================================