/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Oct/2023
 * Time:  10h:34m
 * Description: None
 */
package com.m2_cluster_run.commands
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
import com.common.util.path.Path
import com.m2_cluster_run.common.computingTask.CoordinateComputingTask.runRemoteSSHCommand

import sys.process._
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter, RandomAccessFile}
//=============================================================================
//=============================================================================
object BuildCoordFile extends MyLogger {
  //---------------------------------------------------------------------------
  final val COORD_DATA_INDEX_FILE_NAME  = "coordinate_data_index"
  final val COORD_DATA_FILE_NAME        = "coordinate_data"
  //---------------------------------------------------------------------------
  private final val M2_DEPLOY_DIR  = MyConf.c.getString("Cluster.deployDir.m2")
  private final val TMP_DIR        = MyConf.c.getString("Cluster.tmpDirectory")
  //---------------------------------------------------------------------------
  def generateScript(objectName: String, vr: Double) = {
    info(s"Generate processing script of mpo: '$objectName'")
    val m2_command = s"java -jar m2.jar --query $objectName --no-images --vr $vr"
    val processBuilder = sys.process.Process(Seq("bash", "-c", s"cd $M2_DEPLOY_DIR && $m2_command"))
    processBuilder.!!
  }
  //---------------------------------------------------------------------------
  def build(userObjectName: String
            , objectName: String
            , inputDir:String
            , vr: Double) = {

    //get the path to the files to process
    val pathSeq =  Path.getSortedFileListRecursive(inputDir,
                                                    fileExtension = MyConf.c.getStringSeq("Common.fitsFileExtension"))
    //reset common output directory
    val dir = s"$TMP_DIR/$objectName/"

    //sometimes nfs does not allow to delete dir from current node, usually after a run abort. So delete from remote
    val remoteDir = s"/mayapan/groups/spark/m2_cluster_run/$objectName/"
    runRemoteSSHCommand("uxmal",s"rm -fr $remoteDir && mkdir -p $remoteDir")

    //build coordinate file
    val coordFilePath = s"$dir/$COORD_DATA_INDEX_FILE_NAME"
    val raFile = new RandomAccessFile(new File(coordFilePath), "rw")
    raFile.writeLong(pathSeq.size.toLong)
    raFile.close()

    val fileNameSeqPath = s"$dir/$COORD_DATA_FILE_NAME"
    val bw = new BufferedWriter(new FileWriter(new File(fileNameSeqPath)))
    pathSeq.foreach { path =>
      bw.write(path.getAbsolutePath + "\n")
    }
    bw.close()

    //generate processing script
    generateScript(userObjectName, vr)

    //copy script file
    val command = s"cp $M2_DEPLOY_DIR/$objectName.bash $TMP_DIR/$objectName/"
    command.!

    //info
    info(s"Created coordinate file '$coordFilePath'")
    info(s"Created file name  file '$fileNameSeqPath'")
    info(s"Processing script generated at '$M2_DEPLOY_DIR'")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file BuildCoordFile.scala
//=============================================================================