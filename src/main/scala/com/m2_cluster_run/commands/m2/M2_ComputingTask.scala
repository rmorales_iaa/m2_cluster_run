/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Oct/2023
 * Time:  11h:10m
 * Description: None
 */
package com.m2_cluster_run.commands.m2

//=============================================================================
import com.common.configuration.MyConf
import com.common.util.file.MyFile
import com.m2_cluster_run.commands.BuildCoordFile
import com.m2_cluster_run.common.computingNode.ComputingNode
import com.m2_cluster_run.common.computingNode.ComputingNode.getComputingNodeSeq
import com.m2_cluster_run.common.computingTask.ComputingTask

import java.net.InetAddress
//=============================================================================
import scala.io.Source
//=============================================================================
//=============================================================================
object M2_ComputingTask {
  //---------------------------------------------------------------------------
  private final val TMP_DIR = MyConf.c.getString("Cluster.tmpDirectory")
  //---------------------------------------------------------------------------
  final val PARTIAL_RESULT_DIR  = "partial_result"
  //---------------------------------------------------------------------------
  def getDebugStorageFlag(debugStorage: Boolean) = {
     if (debugStorage) "ON"
     else "OFF"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import M2_ComputingTask._
case class M2_ComputingTask(objectName: String
                            , inputDir: String
                            , threadCount: Option[Int]
                            , debugStorage: Boolean
                            , _dropPhotometry: Boolean
                            , onlyWcsFitting: Boolean
                            , onlyAbsolutePhotometry: Boolean
                            , runAsWorker: Boolean) extends ComputingTask {
  //---------------------------------------------------------------------------
  private val computingNodeSeq = if (runAsWorker) List(ComputingNode(InetAddress.getLocalHost.getHostName))
                                 else getComputingNodeSeq()
  private val coordFilePath = s"$TMP_DIR/$objectName/${BuildCoordFile.COORD_DATA_INDEX_FILE_NAME}"
  private val fileNamePath = s"$TMP_DIR/$objectName/${BuildCoordFile.COORD_DATA_FILE_NAME}"

  assert(MyFile.fileExist(coordFilePath), s"Can not find file: '$coordFilePath'")
  assert(MyFile.fileExist(fileNamePath), s"Can not find file: '$fileNamePath'")

  private val fileNameSeq = Source.fromFile(fileNamePath).getLines().toArray

  //---------------------------------------------------------------------------
  val masterTask = M2_MasterTask(computingNodeSeq
                                 , objectName
                                 , inputDir
                                 , debugStorage
                                 , _dropPhotometry
                                 , onlyWcsFitting
                                 , onlyAbsolutePhotometry)

  val workerTask = M2_WorkerTask(objectName
                                 , coordFilePath
                                 , fileNameSeq
                                 , debugStorage
                                 , onlyWcsFitting
                                 , onlyAbsolutePhotometry)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SourceDetection_ComputingTask.scala
//=============================================================================