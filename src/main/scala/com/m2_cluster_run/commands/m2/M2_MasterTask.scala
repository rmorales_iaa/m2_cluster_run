/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Oct/2023
 * Time:  11h:29m
 * Description: None
 */
package com.m2_cluster_run.commands.m2

//=============================================================================
import com.common.configuration.MyConf
import com.common.util.path.Path
import com.m2_cluster_run.common.computingNode.ComputingNode
import com.m2_cluster_run.common.computingTask.CoordinateComputingTask.runRemoteSSHCommand
import com.m2_cluster_run.common.computingTask.MasterTask
import M2_ComputingTask.{PARTIAL_RESULT_DIR, getDebugStorageFlag}
import com.m2_cluster_run.common.mongoDB.MyMongoDB
//=============================================================================
import sys.process._
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object M2_MasterTask {
  //---------------------------------------------------------------------------
  private final val FINAL_RESULT_DIR          = MyConf.c.getString("Cluster.finalResultDir")
  //---------------------------------------------------------------------------
  private final val TMP_DIR                   = MyConf.c.getString("Cluster.tmpDirectory")
  private final val M2_DEPLOY_DIR             = MyConf.c.getString("Cluster.deployDir.m2")
  private final val M2_CLUSTER_DEPLOY_DIR     = MyConf.c.getString("Cluster.deployDir.cluster_run")
  //---------------------------------------------------------------------------
  private final val M_2_SCRIPT_DIFFERENTIAL_PHOTOMETRY                   = 12
  //---------------------------------------------------------------------------
  private final val IMAGE_METADATA_CSV    = "image_metadata.csv"
  private final val DETAILED_ANALYSIS     = "detailed_analysis"
  private final val DETECTION_SUMMARY_CSV = "detection_summary.csv"

  //---------------------------------------------------------------------------
}
//=============================================================================
import M2_MasterTask._
case class M2_MasterTask(computingNodeSeq: List[ComputingNode]
                         , objectName: String
                         , inputDir: String
                         , debugStorage: Boolean
                         , _dropPhotometry: Boolean
                         , onlyWcsFitting: Boolean
                         , onlyAbsolutePhotometry: Boolean)  extends MasterTask {
  //---------------------------------------------------------------------------
  private val deployDir     = s"$M2_DEPLOY_DIR/output/results/$objectName${if (debugStorage) "_debug" else ""}"
  private val finalResultDir = s"$FINAL_RESULT_DIR/$objectName${if (debugStorage) "_debug" else ""}"
  //---------------------------------------------------------------------------
  def run(nodeCount: Int
          , nodeThreadCount: Int
          , nodeThreadMultiplier: Int
          , _totalThreadCount: Int): Unit = {

    Path.ensureDirectoryExist(s"$TMP_DIR/$objectName/$PARTIAL_RESULT_DIR/")

    if (_dropPhotometry) dropAbsolutePhotometry()

    info(s"Using '$totalNodeCount' computing nodes and '$totalThreadCount' threads for processing tasks")
    info(s"Computing nodes configuration:" + computingNodeSeq.map(_.toString()).mkString("\n\t", "\n\t", ""))

    //clear linux caches
    s"dropcaches_system_ctl".!

    //run workers
    runWorkerSeq()

    //clear linux caches
    s"dropcaches_system_ctl".!

    if (!onlyWcsFitting) {
      jointPartialResults()
      continueWithMasterTask()
    }

    //clear linux caches
    s"dropcaches_system_ctl".!

    info(s"End of computing tasks using '$totalNodeCount' computing nodes and '$totalThreadCount' threads")
  }
  //---------------------------------------------------------------------------
  def runWorker(computingNode: ComputingNode
                , nodeCount: Int
                , nodeThreadCount: Int
                , nodeThreadMultiplier: Int
                , totalThreadCount: Int): Unit = {

    val debugStorageString = if (debugStorage) "--debug-storage" else ""
    val onlyWcsFittingString = if (onlyWcsFitting) "--only-wcs-fitting" else ""
    val onlyPhotometryString = if (onlyAbsolutePhotometry) "--only-photometry" else ""

    val command = s"java -jar m2_cluster_run.jar " +
      s"--m2 $objectName " +
      s"--dir $inputDir " +
      s"$onlyWcsFittingString " +
      s"$onlyPhotometryString " +
      s"$debugStorageString " +
      s"--node-count $nodeCount " +
      s"--node-thread-count $nodeThreadCount " +
      s"--node-thread-multiplier $nodeThreadMultiplier " +
      s"--total-thread $totalThreadCount " +
      s"--worker"

    val finalCommand = s"cd $M2_CLUSTER_DEPLOY_DIR && $command"
    info(s"Running at '${computingNode.nodeName}' remote command '$finalCommand'")
    runRemoteSSHCommand(computingNode.nodeName, finalCommand)
  }
  //---------------------------------------------------------------------------
  private def dropAbsolutePhotometry(): Boolean = {
    val collectionName = objectName + (if (debugStorage) "_debug" else "")
    info(s"$nodeName: deleting calculations from database")
    MyMongoDB("mpo_catalog", collectionName).dropCollection()
    MyMongoDB("mpo_image_catalog", collectionName).dropCollection()
    true
  }
  //---------------------------------------------------------------------------
  private def dropAstrometry(): Boolean = {
    info(s"$nodeName: deleting sub-GAIA previous calculations")
    MyMongoDB("sub_gaia", objectName).dropCollection()
    info(s"$nodeName: creating sub-GAIA")

    val m2_command = s"java -jar m2.jar --sub-gaia $objectName --dir $inputDir"
    val processBuilder = sys.process.Process(Seq("bash", "-c", s"cd $M2_DEPLOY_DIR && $m2_command"))
    processBuilder.!
    true
  }
  //---------------------------------------------------------------------------
  def jointPartialResults(): Unit = {
    Path.resetDirectory(deployDir)
    jointPartialDetectionSummarySeq()
    jointPartialDetailedAnalysis()
  }
  //---------------------------------------------------------------------------
  private def jointPartialDetectionSummarySeq(): Unit = {

    info("Master task: building the detection summary")
    val partialResultDir = s"$TMP_DIR/$objectName/$PARTIAL_RESULT_DIR/"
    val csvSeq = Path.getSortedFileListRecursive(
      partialResultDir
      , pattern = Some(s".*$DETECTION_SUMMARY_CSV.*")
      , fileExtension = Seq(".csv"))

    //create the output csv
    val dir = s"$deployDir/0_source_detection/"
    Path.ensureDirectoryExist(dir)
    val outputCsv = s"$dir/detection_summary.csv"

    //copy the first csv
    val command = s"cp ${csvSeq.head.getAbsolutePath} $outputCsv"
    s"$command".!

    //append to end the csv
    val bw = new BufferedWriter(new FileWriter(new File(outputCsv), true))

    //copy the remain csv
    csvSeq.drop(1).foreach { path =>
      val csv = scala.io.Source.fromFile(path.getAbsolutePath)
      csv.getLines().drop(1).foreach(line => bw.write(line + "\n"))
      csv.close()
    }

    bw.close()
  }
  //---------------------------------------------------------------------------
  private def jointPartialDetailedAnalysis(): Unit = {

    info("Master task: building the detailed analysis directory")
    val partialResultDir = s"$TMP_DIR/$objectName/$PARTIAL_RESULT_DIR/"
    val dirSeq = Path.getSortedSubdirectoryListRecursive(
      partialResultDir)
      .filter(_.endsWith(DETAILED_ANALYSIS))

    val outputDir = s"$deployDir/0_source_detection/"
    Path.ensureDirectoryExist(outputDir)

    dirSeq.foreach { dir=>
      s"cp -r $dir $outputDir ".!
    }
  }
  //---------------------------------------------------------------------------
  def continueWithMasterTask() = {

    info("Continuing the master task")

    //absolute photometry report and light curve
    info("Master task: absolute photometry report and light curve")
    val debugStorageFlag = getDebugStorageFlag(debugStorage)
    var m2_command = s"./$objectName.bash ${M2_WorkerTask.M_2_SCRIPT__STEP_ABSOLUTE_PHOTOMETRY_REPORT_LIGHT_CURVE} $debugStorageFlag"
    var processBuilder = sys.process.Process(Seq("bash", "-c", s"cd $TMP_DIR/$objectName/ && $m2_command"))
    processBuilder.!

    if (!onlyAbsolutePhotometry) {
      //differential photometry
      info("Master task: calculating differential photometry")
      m2_command = s"./$objectName.bash $M_2_SCRIPT_DIFFERENTIAL_PHOTOMETRY $debugStorageFlag"
      processBuilder = sys.process.Process(Seq("bash", "-c", s"cd $TMP_DIR/$objectName/ && $m2_command"))
      processBuilder.!
    }

    //copy result to final dir
    Path.deleteDirectoryIfExist(finalResultDir)
    s"cp -r $deployDir $finalResultDir".!!
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SourceDetection_MasterTask.scala
//=============================================================================