/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Oct/2023
 * Time:  11h:29m
 * Description: None
 */
package com.m2_cluster_run.commands.m2
//=============================================================================
import com.common.configuration.MyConf
import com.common.util.path.Path
import M2_ComputingTask.{PARTIAL_RESULT_DIR, getDebugStorageFlag}
import com.m2_cluster_run.common.computingTask.{CoordinateComputingTask, WorkerTask}
//=============================================================================
import sys.process._
import java.io.File
//=============================================================================
//=============================================================================
object M2_WorkerTask {
  //---------------------------------------------------------------------------
  private final val TMP_DIR           = MyConf.c.getString("Cluster.tmpDirectory")
  private final val M2_DEPLOY_DIR     = MyConf.c.getString("Cluster.deployDir.m2")
  //---------------------------------------------------------------------------
  private final val M_2_SCRIPT_STEP_SUB_GAIA_WCS_FITTING_ABSOLUTE_PHOTOMETRY  = -1
  private final val M_2_SCRIPT_STEP_ONLY_WCS_FITTING                          = -2
  private final val M_2_SCRIPT_STEP_ONLY_ABSOLUTE_PHOTOMETRY                  = -3
  final val M_2_SCRIPT__STEP_ABSOLUTE_PHOTOMETRY_REPORT_LIGHT_CURVE           = -4
  //---------------------------------------------------------------------------
}
//=============================================================================
import  M2_WorkerTask._
case class M2_WorkerTask(objectName: String
                         , coordFilePath: String
                         , fileNameSeq: Array[String]
                         , debugStorage: Boolean
                         , onlyWcsFitting: Boolean
                         , onlyAbsolutePhotometry: Boolean) extends WorkerTask {
  //---------------------------------------------------------------------------
  def run(nodeCount: Int
          , nodeThreadCount: Int
          , nodeThreadMultiplier: Int
          , totalThreadCount: Int): Unit = {
    CoordinateComputingTask.run(
      coordFilePath
      , processFileNameSeq
      , nodeCount
      , nodeThreadCount
      , nodeThreadMultiplier
      , totalThreadCount
      , MyConf.c.getInt("Cluster.ThreadRandomWait.minRandomWaitMs")
      , MyConf.c.getInt("Cluster.ThreadRandomWait.maxRandomWaitMs"))
  }

  //---------------------------------------------------------------------------
  private def processFileNameSeq(minPos: Long
                                , maxPos: Long): Unit = {

    //get the list of images to process
    val pathSeq = for (i <- minPos to maxPos) yield fileNameSeq(i.toInt - 1)

    //create a tmp dir for the images to process
    val tmpImageDir = s"$TMP_DIR/$objectName/${nodeName}_${minPos}_$maxPos"
    Path.resetDirectory(tmpImageDir)

    //link the images
    pathSeq.foreach { path =>
      s"ln -s $path $tmpImageDir".!
    }

    //reset local output directory
    val deployDir = s"$M2_DEPLOY_DIR/output/results/$objectName${if (debugStorage) "_debug" else ""}"
    Path.resetDirectory(deployDir)

    //run the m2 script
    run_M2_Script(objectName, tmpImageDir, onlyWcsFitting, onlyAbsolutePhotometry)

    //clear linux caches
    s"dropcaches_system_ctl".!

    //copy the partial results from local to common storage
    val partialResultDestPath = s"$TMP_DIR/$objectName/$PARTIAL_RESULT_DIR/$nodeName/"
    val runCount = if (!Path.directoryExist(partialResultDestPath)) 0
                   else new File(partialResultDestPath).listFiles().filter(_.isDirectory).length

    val finalDestPath = s"$partialResultDestPath/run_${f"$runCount%04d"}"
    s"mkdir -p $finalDestPath".!
    s"cp -r $deployDir $finalDestPath".!

    //delete tmp dir
    Path.deleteDirectoryIfExist(tmpImageDir)
  }

  //---------------------------------------------------------------------------
  private def run_M2_Script(objectName: String
                            , imageDir: String
                            , onlyWcsFitting: Boolean
                            , onlyAbsolutePhotometry: Boolean) = {
    info(s"$nodeName: running data processing: '$objectName' with '$imageDir'")

    //copy m2 script to local storage
    info("Copying m2 script to local storage")
    val m2_sourceScript = s"$TMP_DIR/$objectName/$objectName.bash"
    val m2_destScript = s"$M2_DEPLOY_DIR/$objectName.bash"
    s"cp $m2_sourceScript $m2_destScript".!

    //run it
    val debugStorageFlag = getDebugStorageFlag(debugStorage)
    val scriptOption = {
      if (onlyWcsFitting) M_2_SCRIPT_STEP_ONLY_WCS_FITTING
      else {
        if (onlyAbsolutePhotometry) M_2_SCRIPT_STEP_ONLY_ABSOLUTE_PHOTOMETRY
        else M_2_SCRIPT_STEP_SUB_GAIA_WCS_FITTING_ABSOLUTE_PHOTOMETRY
      }
    }
    val m2_command = s"./$objectName.bash $scriptOption $debugStorageFlag $imageDir"
    val processBuilder = sys.process.Process(Seq("bash", "-c", s"cd $M2_DEPLOY_DIR && $m2_command"))
    processBuilder.!
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SourceDetection_WorkerTask.scala
//=============================================================================