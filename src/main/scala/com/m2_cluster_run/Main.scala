/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Feb/2020
 * Time:  15h:19m
 * Description: None
 */
//=============================================================================
package com.m2_cluster_run
//=============================================================================
import BuildInfo.BuildInfo
import com.common.configuration.MyConf
import com.common.logger.MyLogger
import com.common.util.path.Path
import com.m2_cluster_run.commandLine.CommandLineParser
//=============================================================================
//=============================================================================
object Main extends MyLogger {
  //---------------------------------------------------------------------------
  def main(args: Array[String]): Unit = {

  //init output directory
    Path.ensureDirectoryExist("output/")

    val userArgs = if (args.isEmpty) Array("--help") else args
    val cl = new CommandLineParser(userArgs)
    MyConf.c = MyConf("input/configuration/main.conf")
    if (MyConf.c == null || !MyConf.c.isLoaded) fatal(s"Error. Error parsing configuration file")

    info(s"----------------------------- m2_cluster_run '${BuildInfo.version}' starts -----------------------------")
    m2_cluster_run().run(cl)
    info(s"----------------------------- m2_cluster_run '${BuildInfo.version}' ends -------------------------------")
    System.exit(0)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Main.scala
//=============================================================================
