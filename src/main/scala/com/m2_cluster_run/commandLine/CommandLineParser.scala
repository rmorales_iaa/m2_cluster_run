/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jun/2023
 * Time:  20h:40m
 * Description: None
 */
package com.m2_cluster_run.commandLine
//=============================================================================
import com.m2_cluster_run.Version
import com.common.logger.MyLogger
//=============================================================================
import org.rogach.scallop._
//=============================================================================
//=============================================================================
object CommandLineParser {
  //---------------------------------------------------------------------------
  final val COMMAND_VERSION                 = "version"
  final val COMMAND_M2                      = "m2"
  //---------------------------------------------------------------------------
  final val VALID_COMMAND_SEQ = Seq(
      COMMAND_VERSION
    , COMMAND_M2
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
import CommandLineParser._
class CommandLineParser(args: Array[String]) extends ScallopConf(args) with MyLogger {
  version(Version.value + "\n")
  banner(s"""sparkFits syntax
            |sparkFits [configurationFile][command][parameters]
            |[commnads and parameters]
            |\t[$COMMAND_VERSION]
            |\t[$COMMAND_M2]
            | #---------------------------------------------------------------------------------------------------------
            |Example 0: Show version
            |  java -jar sparkFits.jar --$COMMAND_VERSION
            | #---------------------------------------------------------------------------------------------------------
            |Example 1: Build the set of files used to coordinate all threads in all nodes in the cluster.
            |  It contains the number of files (in binary) of directory 'd' (that also must be in a common storage).
            | 'n' is the object name present in all images of 'd'. The file is located as it is specified in configuration
            |  entry 'Cluster.tmpDirectory'. The name of the file is 'coordinate_file'. Also, inside the same
            |  directory is created the file "filename_seq" containing the path to the files of 'd'. A processing script
            |  is created ant m2 deploy directory. The V-R parameter is '0.432'
            |  Finally all workers nodes defined in the configuration file are used to complete the task
            |
            |  java -jar m2_cluster_run.jar --$COMMAND_M2 n --dir d --vr 0.432
            |
            |  #---------------------------------------------------------------------------------------------------------
            |Example 1.1: Same as 1 but dropping all previous calculations
            |
            |  java -jar m2_cluster_run.jar --$COMMAND_M2 n --dir d --drop-all
            |
            | #---------------------------------------------------------------------------------------------------------
            |Example 1.2: Same as 1 but dropping only photometry
            |
            |  java -jar m2_cluster_run.jar --$COMMAND_M2 n --dir d --drop-photometry
            | #---------------------------------------------------------------------------------------------------------
            |Example 1.3: Same as 1 but using debug storage in the database
            |
            |  java -jar m2_cluster_run.jar --$COMMAND_M2 n --dir d --debug-storage
            |#---------------------------------------------------------------------------------------------------------
            |Example 1.4: Same as 1 but using calculating only photometry absolute and differential (no sub-GAIA neither wcs fitting)
            |
            |  java -jar m2_cluster_run.jar --$COMMAND_M2 n --dir d --only-photometry
            |
            |#---------------------------------------------------------------------------------------------------------
            |Example 1.5: Same as 1 but only calculating the wcs fitting
            |
            |  java -jar m2_cluster_run.jar --$COMMAND_M2 n --dir d --only-wcs-fitting
            |
            |#---------------------------------------------------------------------------------------------------------
            |Example 1.6: Same as 1 but indicating that run as computing node worker using 't' as max number of threads
            | to be used in the node,  'z' as total number of threads in all nodes and 'x' as number of nodes running
            | to complete the task. The number 'm' multiplied by thread count indicates the size of the chunk to be
            | reduced in the working node
            |
            |  java -jar m2_cluster_run.jar --$COMMAND_M2 n --dir d --node-count x --node-thread-count t  --node-thread-multiplier m --totalThread z --worker
            |
            | #---------------------------------------------------------------------------------------------------------
            |Options:
            |""".stripMargin)
  footer("\nFor detailed information, please consult the documentation!")
  //---------------------------------------------------------------------------
  val version = opt[Boolean](
      required = false
    , noshort = true
    , descr = "Program version\n"
  )
  //---------------------------------------------------------------------------
  val m2 = opt[String](
    required = false
    , noshort = true
    , descr = "Program version\n"
  )
  //---------------------------------------------------------------------------
  val dir = opt[String](
    required = false
    , noshort = true
    , descr = "Input directory\n"
  )
  //---------------------------------------------------------------------------
  val vr = opt[Double](required = false
    , short = 'v'
    , default = Some(0.5d)
    , descr = "Photometric V-R value\n")
  //---------------------------------------------------------------------------
  val dropPhotometry = opt[Boolean](
    required = false
    , noshort = true
    , descr = "drop only photometry calculations\n"
  )
  //---------------------------------------------------------------------------
  val debugStorage = opt[Boolean](
    required = false
    , noshort = true
    , descr = "use the debug storage in the database\n"
  )
  //---------------------------------------------------------------------------
  val onlyWcsFitting = opt[Boolean](
    required = false
    , noshort = true
    , descr = "calculate only wcs fitting\n"
  )
  //---------------------------------------------------------------------------
  val onlyPhotometry = opt[Boolean](
    required = false
    , noshort = true
    , descr = "calculate only photometry absolute and differential (no sub-GAIA neither wcs fitting)\n"
  )

  //---------------------------------------------------------------------------
  val nodeCount = opt[Int](
    required = false
    , noshort = true
    , descr = "number of nodes to complete the task\n"
  )
  //---------------------------------------------------------------------------
  val nodeThreadCount = opt[Int](
    required = false
    , noshort = true
    , descr = "number of threads to be used\n"
  )
  //---------------------------------------------------------------------------
  val nodeThreadMultiplier = opt[Int](
    required = false
    , noshort = true
    , descr = "this number multiplied by thread count indicates the size of the chunk to be reduced in the working node\n"
  )
  //---------------------------------------------------------------------------
  val totalThread = opt[Int](
    required = false
    , noshort = true
    , descr = "total number of threads to be used\n"
  )

  //---------------------------------------------------------------------------
  val worker = opt[Boolean](
    required = false
    , noshort = true
    , descr = "run as worker\n"
  )
  //---------------------------------------------------------------------------
  val commandSeq = Seq(version, m2)
  mutuallyExclusive(commandSeq:_*)
  requireOne       (commandSeq:_*)
  verify()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CommandLineParser.scala
//=============================================================================
